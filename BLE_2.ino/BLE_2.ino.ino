#include "BLEDevice.h"
#include "BLEScan.h"
#include "WiFi.h"
#include "HTTPClient.h"
#include "heltec.h"


const char* ssid = "embiq";
const char* password =  "$Embiq$.biuro";
// network ssid and password

// The remote service we wish to connect to.
static BLEUUID serviceUUID("0000AD00-B35C-11E4-9813-0002A5D5C51B"); //main service UUID
// The characteristic of the remote service we are interested in.
static BLEUUID    charUUID03("0000AD03-B35C-11E4-9813-0002A5D5C51B");  // write to take notification 
static BLEUUID    charUUID02("0000AD02-B35C-11E4-9813-0002A5D5C51B");  //take notification 
static BLEUUID    charUUID06("0000AD06-B35C-11E4-9813-0002A5D5C51B");  //read

//static BLEUUID serviceUUID("0000fff0-0000-1000-8000-00805f9b34fb"); //MEDEA main service
//static BLEUUID service1UUID("0000ffe0-0000-1000-8000-00805f9b34fb"); //MEDEA password service 
//static BLEUUID service2UUID("0000ff60-0000-1000-8000-00805f9b34fb"); //MEDEA game service
//static BLEUUID    charUUID("0000FFF3-0000-1000-8000-00805F9B34FB");//MEDEA write to take notify
//static BLEUUID    charUUID2("0000FF75-0000-1000-8000-00805F9B34FB");//MEDEA next round
//static BLEUUID    charUUID4("0000FF62-0000-1000-8000-00805F9B34FB");//MEDEA first question
//static BLEUUID    charUUID2("0000FFF4-0000-1000-8000-00805F9B34FB");//MEDEA notification
//static BLEUUID    charPASS("0000FFE1-0000-1000-8000-00805F9B34FB");//MEDEA password


std::string value;
BLERemoteService* pRemoteService;

BLEClient*  pClient;
BLEScan* pBLEScan = BLEDevice::getScan();


static boolean doConnect = false;
static boolean connected = false;
static boolean doScan = false;
static BLERemoteCharacteristic* pRemoteCharacteristicAD03;
static BLERemoteCharacteristic* pRemoteCharacteristic02;
static BLERemoteCharacteristic* pRemoteCharacteristic06;
static BLEAdvertisedDevice* myDevice;
int Incoming_value=0;
int arry[3];

class MySecurity : public BLESecurityCallbacks {

  uint32_t onPassKeyRequest(){
    return 123456;
  }
  void onPassKeyNotify(uint32_t pass_key){
        ESP_LOGE(LOG_TAG, "The passkey Notify number:%d", pass_key);
  }
  bool onConfirmPIN(uint32_t pass_key){
        ESP_LOGI(LOG_TAG, "The passkey YES/NO number:%d", pass_key);
      vTaskDelay(5000);
    return true;
  }
  bool onSecurityRequest(){
    ESP_LOGI(LOG_TAG, "Security Request");
    return true;
  }
  void onAuthenticationComplete(esp_ble_auth_cmpl_t auth_cmpl){
    if(auth_cmpl.success){
      ESP_LOGI(LOG_TAG, "remote BD_ADDR:");
     // esp_log_buffer_hex(LOG_TAG, auth_cmpl.bd_addr, sizeof(auth_cmpl.bd_addr));
      ESP_LOGI(LOG_TAG, "address type = %d", auth_cmpl.addr_type);
    }
        ESP_LOGI(LOG_TAG, "pair status = %s", auth_cmpl.success ? "success" : "fail");
  }
};

static void notifyCallback( BLERemoteCharacteristic* pBLERemoteCharacteristic,
                            uint8_t* pData,  size_t length,  bool isNotify ) {
        Serial.println("Notify here :D");
        Heltec.display -> clear();
        Heltec.display -> drawString(0, 0, "Notify here :D");
        Heltec.display -> display();
  //Serial.println(pData[0],HEX);
  for(uint8_t i=0; i<length; i++){
    Serial.print(*pData, HEX);
    pData++;  
  }
  Serial.println("");
}
class MyClientCallback : public BLEClientCallbacks {
  void onConnect(BLEClient* pclient) {
  }
  void onDisconnect(BLEClient* pclient) {
    connected = false;
    Serial.println("onDisconnect");
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Disconnect");
    Heltec.display -> display();
  }
};
bool connectToServer() {
    Serial.print("Forming a connection to ");
    Serial.println(myDevice->getAddress().toString().c_str());

        BLEDevice::setEncryptionLevel(ESP_BLE_SEC_ENCRYPT);
        BLEDevice::setSecurityCallbacks(new MySecurity());
      
        BLESecurity *pSecurity = new BLESecurity();
        pSecurity->setAuthenticationMode(ESP_LE_AUTH_REQ_SC_ONLY);
        pSecurity->setCapability(ESP_IO_CAP_OUT);
        pSecurity->setRespEncryptionKey(ESP_BLE_ENC_KEY_MASK | ESP_BLE_ID_KEY_MASK);
    
    pClient  = BLEDevice::createClient();
    Serial.println(" - Created client");
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Created BLE client");
    Heltec.display -> display();
    
   // pClient->setClientCallbacks(new MyClientCallback());
    // Connect to the remove BLE Server.
    pClient->connect(myDevice);  // if you pass BLEAdvertisedDevice instead of address, it will be recognized type of peer device address (public or private)
    Serial.println(" - Connected to server");
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Connected to BLE server");
    Heltec.display -> display();
    //connected=true;
    
    // Obtain a reference to the service we are after in the remote BLE server.
    pRemoteService = pClient->getService(serviceUUID);
    pRemoteService = pClient->getService(serviceUUID);
    if (pRemoteService == nullptr) {
      Serial.print("Failed to find our service UUID: ");
      Serial.println(serviceUUID.toString().c_str());
      Heltec.display -> clear();
      Heltec.display -> drawString(0, 0, "Failed to find our service UUID: ");
      Heltec.display -> drawString(0, 10, serviceUUID.toString().c_str());
      Heltec.display -> display();
    
      
      pClient->disconnect();
      return false;
    }
    Serial.println(" - Found our service");
}
bool writeCharacteristic(){
    // Obtain a reference to the characteristic in the service of the remote BLE server.
    pRemoteService = pClient->getService(serviceUUID);
    pRemoteCharacteristicAD03 = pRemoteService->getCharacteristic(charUUID03);
    if (pRemoteCharacteristicAD03 == nullptr) {
      Serial.print("Failed to find our characteristic UUID: ");
      Serial.println(charUUID03.toString().c_str());
      Heltec.display -> clear();
      Heltec.display -> drawString(0, 0, "Failed to find our characteristic UUID: ");
      Heltec.display -> drawString(0, 10, charUUID03.toString().c_str());
      Heltec.display -> display();
      delay(500);
      
      pClient->disconnect();
      return false;
    }
    Serial.println(" - Found our characteristic");
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Found our characteristic");
    Heltec.display -> display();
    delay(500);
    //connected = true;
      //String newValue = "0x53 48 55 6d 02 34 f0 45 e1 b0 45";
      uint8_t message[] ={0x53, 0x48, 0x55, 0x6d, 0x02, 0x34, 0xf0, 0x45, 0xe1, 0xb0, 0x45};
                         // 53   48   55       6D    2   34    F0    45    E1    B0    45

      uint8_t mess[]= {0x14};
    if(pRemoteCharacteristicAD03->canWrite()) {
      pRemoteCharacteristicAD03->writeValue(message,11,true);
      Serial.println("The value sent to characteristic ");
      Heltec.display -> clear();
      Heltec.display -> drawString(0, 0, "The value sent");
      Heltec.display -> display();
      delay(500);
     }
    else {
      Serial.print("Can't write");
      Heltec.display -> clear();
      Heltec.display -> drawString(0, 0, "Can't write");
      Heltec.display -> display();
      delay(500);
    }
    // Read the value of the characteristic.
   /* if(pRemoteCharacteristic->canRead()) {
      int16_t value = pRemoteCharacteristic->readUInt16();
      Serial.print("The characteristic value was: ");
      Serial.println(value);
      Heltec.display -> clear();
      Heltec.display -> drawString(0, 0, "The characteristic value was: ");
      Heltec.display -> drawString(0,6, value.toString().c_str());
      Heltec.display -> display(); 
    */
  //  pRemoteService = pClient->getService(serviceUUID);
    pRemoteCharacteristic06 = pRemoteService->getCharacteristic(charUUID06);
    if (pRemoteCharacteristic06 == nullptr) {
      Serial.print("Failed to find our characteristic UUID02: ");
      Serial.println(charUUID02.toString().c_str());
      
      pClient->disconnect();
      return false;
    }
    
      Serial.println(" - Found our characteristic- name");
      Heltec.display -> clear();
      Heltec.display -> drawString(0, 0, "Found  characteristic - name ");
     // Heltec.display -> drawString(0, 11, "round");
      Heltec.display -> display();
    // Read the name of device.
    if(pRemoteCharacteristic06->canRead()) {
      // Read the value of the characteristic.
      value = pRemoteCharacteristic06->readValue();
      uint8_t val = pRemoteCharacteristic06->readUInt8();
      Serial.print("Name is: ");
      Serial.println(value.c_str());
      Serial.println(value.length());
      
      if (value.length() > 0) {
        for (int i = 0; i < value.length(); i++)
          Serial.print(value[i], HEX);
        Serial.println();
        
      }
    
         
    } 
  /*  uint8_t array1[1] = { 0x01};
    
    if(pRemoteCharacteristic->canWrite()) {
      pRemoteCharacteristic->writeValue((byte)0x01);
      Serial.println("The value sent to characteristic next round ");
    } else Serial.println("Can't write");
    
    pRemoteCharacteristic = pRemoteService->getCharacteristic(charUUID4);
    if (pRemoteCharacteristic == nullptr) {
      Serial.print("Failed to find our characteristic UUID2: ");
      Serial.println(charUUID2.toString().c_str());
      
      pClient->disconnect();
      return false;
    }
    
      Serial.println(" - Found our characteristic- next rouund");
      Heltec.display -> clear();
      Heltec.display -> drawString(0, 0, "Found  characteristic first ");
      Heltec.display -> drawString(0, 11, "question");
      Heltec.display -> display();
   /* // Read the name of device.
    if(pRemoteCharacteristic->canRead()) {
      FFF5_value = pRemoteCharacteristic->readValue();
      Serial.print("The characteristic 2 FFF_value was: ");
         Serial.println(FFF5_value.c_str());
    } 
    
    if(pRemoteCharacteristic->canWrite()) {
      pRemoteCharacteristic->writeValue((byte)0x01);
      Serial.println("The value sent to characteristic first question ");
    } else Serial.println("Can't write"); */
    
     pRemoteCharacteristic02 = pRemoteService->getCharacteristic(charUUID02);
      if (pRemoteCharacteristic02 == nullptr) {
        Serial.print("Failed to find our characteristic UUID2: ");
        Serial.println(charUUID02.toString().c_str());
        Heltec.display -> clear();
        Heltec.display -> drawString(0, 0, "Failed to find our characteristic ");
        Heltec.display -> drawString(0, 10, charUUID02.toString().c_str());
        Heltec.display -> display();
        delay(1000);
        pClient->disconnect();
        return false;
      }
      
        Serial.println(" - Found our characteristic notify ");
        Heltec.display -> clear();
        Heltec.display -> drawString(0, 0, "Found our characteristic");
         Heltec.display -> drawString(0, 11, " notify");
        Heltec.display -> display();
        delay(500);
        
    if(pRemoteCharacteristic02->canNotify()){
       pRemoteCharacteristic02->registerForNotify(notifyCallback);
       Serial.println("Notify port detected");
        Heltec.display -> clear();
        Heltec.display -> drawString(0, 0, "Notify port deteted");
        Heltec.display -> display();
        delay(500);
    }
   
      return true;
} 
/**
 * Scan for BLE servers and find the first one that advertises the service we are looking for.
 */
class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
 /**
   * Called for each advertising BLE server.
   */
  void onResult(BLEAdvertisedDevice advertisedDevice) {
    Serial.print("BLE Advertised Device found: ");
    Serial.println(advertisedDevice.toString().c_str());
    // We have found a device, let us now see if it contains the service we are looking for.
    if (advertisedDevice.haveServiceUUID() && advertisedDevice.isAdvertisingService(serviceUUID)) {
      BLEDevice::getScan()->stop();
      myDevice = new BLEAdvertisedDevice(advertisedDevice);
      doConnect = true;
      doScan = true;
    } // Found our server
  } // onResult
}; // MyAdvertisedDeviceCallbacks
void setup() {
  Serial.begin(115200);
  Heltec.begin(true /*DisplayEnable Enable*/, false /*LoRa Enable*/, true /*Serial Enable*/);
  delay(2000);
  connected=false;
  
  Serial.println("Starting Arduino BLE Client application...");
  Heltec.display -> clear();
  Heltec.display -> drawString(0, 0, "Starting Arduino BLE Client application... ");
  Heltec.display -> display();
  
  BLEDevice::init("");
  // Retrieve a Scanner and set the callback we want to use to be informed when we
  // have detected a new device.  Specify that we want active scanning and start the
  // scan to run for 5 seconds.
  
    // If the flag "doConnect" is true then we have scanned for and found the desired
  // BLE Server with which we wish to connect.  Now we connect to it.  Once we are 
  // connected we set the connected flag to be true.
 while(connected==false){  
  Serial.println("BLE device not found. Scanning again...");
  Heltec.display -> clear();
  Heltec.display -> drawString(0, 0, "BLE device not found.");
  Heltec.display -> drawString(0, 11, "Scanning again...");
  Heltec.display -> display();
   
   pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
   pBLEScan->setInterval(1349);
   pBLEScan->setWindow(449);
   pBLEScan->setActiveScan(true);
   pBLEScan->start(5, false); 
   pBLEScan->start(5, false);
 
  if (doConnect == true) {
   
      if (connectToServer()) {
        Serial.println("We are now connected to the BLE Server.");
        Heltec.display -> clear();
        Heltec.display -> drawString(0, 0, "We are now connected to ");
        Heltec.display -> drawString(0, 11, "the BLE Server.");
        Heltec.display -> display();
        connected= true;
    
   // doConnect = false;
  }
}
 }
 /* WiFi.begin(ssid, password); 
 
  while (WiFi.status() != WL_CONNECTED) { //Check for the connection
    delay(1000);
    Serial.println("Connecting to WiFi..");
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Connecting to WiFi..");
    Heltec.display -> display();
  }
  delay(1000);
  Heltec.display -> clear();
 
  Serial.println("Connected to the WiFi network");
  Heltec.display -> drawString(0, 0, "Connected to the WiFi network");
  Heltec.display -> display();
  */
} // End of setup.
// This is the Arduino main loop function.
void loop() {
  
  writeCharacteristic();
  /*  if(Serial.available() > 0)  
    { 
      for(int i=0;3;i++){
        Incoming_value = Serial.read();  
        arry[i]=Incoming_value;
        //Read the incoming data and store it into variable Incoming_value
      }
      for(int i=0;i<3;i++)
      {
          Serial.print(arry[i]);
          Serial.print("\n");//Print Value of Incoming_value in Serial monitor
      }
    } 
      else {
     // Serial.println("We have failed to connect to the server; there is nothin more we will do.");
      }
  */
  delay(500); // Delay a second between loops.
 /* if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
  
   HTTPClient http;   
   http.begin("http://10.11.40.6:5000/test_api");  //Specify destination for HTTP request
   http.addHeader("Content-Type", "text/plain");             //Specify content-type header
  
   int httpResponseCode = http.POST(FFF5_value.c_str());   //Send the actual POST request
    Serial.println(FFF5_value.c_str());
    
   if(httpResponseCode>0){
  
    String response = http.getString();                       //Get the response to the request
  
    Serial.println(httpResponseCode);   //Print return code
    Serial.println(response);           //Print request answer
  
   }else{
  
    Serial.print("Error on sending POST: ");
    Serial.println(httpResponseCode);
  
   }
  
   http.end();  //Free resources
  
 }else{
  
    Serial.println("Error in WiFi connection");
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Error in WiFi connection");
    Heltec.display -> display();   
  
 } */
  
  //delay(5000);  //Send a request every 5 seconds
  
}
